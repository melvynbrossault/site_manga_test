<?php

class Scan_model extends CI_Model{

	/* -- Constructeur -- */
	public function __construct()
        {
            parent::__construct();

            $this->load->helper('url');
            $this->load->database();
            $this->db->set('');
        }

	public function get_last_scan()
        {
            $this->db->select('mangas.nom, chapitre, date_mise_en_ligne, mangas.id');
			$this->db->from('scan');
			$this->db->join('mangas','mangas.id = scan.id_manga');
            $this->db->order_by('date_mise_en_ligne', 'DESC');
            $query = $this->db->get();
            return $query->result_array();
    }

    public function get_scan()
        {
            $this->db->select('mangas.nom, chapitre, date_mise_en_ligne, mangas.id');
			$this->db->from('scan');
			$this->db->join('mangas','mangas.id = scan.id_manga');
            $this->db->order_by('date_mise_en_ligne', 'ASC');
            $query = $this->db->get();
            return $query->result_array();
    }

    public function get_mangas()
        {
            $this->db->select('id, nom, nom_alternatif, origine, date_sortie, type, genre, auteur, synopsis');
			$this->db->from('mangas');
            $this->db->order_by('nom', 'ASC');
            $query = $this->db->get();
            return $query->result_array();
    }


    public function ajout_correct() {
        $id = $this->input->post('nom');
        settype($id, "integer");
        $chap = $this->input->post('chapitre');
        settype($chap, "integer");
        $ajout = array(
            'id_manga' => $id,
            'chapitre' => $chap
        );

        $this->db->insert('scan', $ajout);
    }
}
	
?>