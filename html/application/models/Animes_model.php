<?php

class Animes_model extends CI_Model{

	/* -- Constructeur -- */
	public function __construct()
        {
            parent::__construct();

            $this->load->helper('url');
            $this->load->database();
            $this->db->set('');
        }

	public function get_last_episodes()
        {
            $this->db->select('animes.nom, animes.langue, episode, date_mise_en_ligne, animes.id');
			$this->db->from('episodes');
			$this->db->join('animes','animes.id = episodes.id_anime');
            $this->db->order_by('date_mise_en_ligne', 'DESC');
            $query = $this->db->get();
            return $query->result_array();
    }

    public function get_episodes()
        {
            $this->db->select('animes.nom, animes.langue, episode, date_mise_en_ligne, animes.id, lien_1, lien_2, animes.synopsis, animes.image, animes.auteur, animes.type, animes.genre, animes.parution');
			$this->db->from('episodes');
			$this->db->join('animes','animes.id = episodes.id_anime');
            $this->db->order_by('date_mise_en_ligne', 'ASC');
            $query = $this->db->get();
            return $query->result_array();
    }

    public function get_animes()
        {
            $this->db->select('nom, langue, id, image');
			$this->db->from('animes');
            $this->db->order_by('nom', 'ASC');
            $this->db->order_by('langue', 'ASC');
            $query = $this->db->get();
            return $query->result_array();
    }

    public function get_anime_id($nom, $langue)
        {
            $query = "SELECT id from animes where nom = '$nom' and langue = '$langue'";
            $res = $this->db->query($query);
            return $res->result_array();
    }

    public function ajout_correct_anime() {
        $nom = $this->input->post('nom');
        $langue = $this->input->post('langue');
        $auteur = $this->input->post('auteur');
        $type = $this->input->post('type');
        $genre = $this->input->post('genre');
        $synospis = $this->input->post('synopsis');
        $parution = $this->input->post('parution');
        $sql = "INSERT INTO `animes` (`id`, `nom`, `langue`, `image`, `auteur`, `type`, `genre`, `synopsis`, `parution`) values (null, '$nom', '$langue', null,'$auteur', '$type', '$genre', '$synospis', '$parution')";
        $this->db->query($sql);
    }

    public function ajout_correct_episode() {
        $id =  $this->Animes_model->get_anime_id($this->input->post('nom'),$this->input->post('langue'));
        echo $id;
        $episode = $this->input->post('episode');
        $lien_1 = $this->input->post('lien_1');
        $lien_2 = $this->input->post('lien_2');
        $sql = "INSERT INTO `episodes` (`id`, `id_anime`, `episode`, `lien_1`, `lien_2`, `date_mise_en_ligne`) VALUES (NULL, '$id', '$episode', '$lien_1', '$lien_2', CURRENT_TIMESTAMP)";
        $this->db->query($sql);
    }
}
	
?>