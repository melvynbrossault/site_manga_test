<?php

class User_model extends CI_Model{

	/* -- Constructeur -- */
	public function __construct()
        {
            parent::__construct();

            $this->load->helper('url');
            $this->load->database();
            $this->db->set('');
        }

	public function get_user()
        {
            $this->db->select('username, password, id, date_inscription, niveau');
			$this->db->from('membres');
            $query = $this->db->get();
            return $query->result_array();
    }
}
	
?>