<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accueil extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Animes_model');
		$this->load->model('Scan_model');
		$this->load->model('User_model');
		$this->load->helper('url');
	}
	
    public function index() {
		$info['utilisateur']=$this->User_model->get_user();
        $info['content']='utilisateur';
                
		$data['list_last_episodes']=$this->Animes_model->get_last_episodes();
		$data['content']='list_last_episodes';

		$scan['list_last_scan']=$this->Scan_model->get_last_scan();
		$scan['content']='list_last_scan';
		
		$this->load->vars($info);
		$this->load->vars($data);
		$this->load->vars($scan);
		$this->load->view('header');
		$this->load->view('accueil');
		$this->load->view('footer');
	}

	function admin_view()
	{
        $info['utilisateur']=$this->User_model->get_user();
        $info['content']='utilisateur';
		
		$this->load->vars($info);
        $this->load->view('header');
		$this->load->view('admin_view');
        $this->load->view('footer');
	}
}
?>
