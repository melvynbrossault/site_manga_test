<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class All_anime extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Animes_model');
		$this->load->model('User_model');
		$this->load->helper('url');
	}
	
    public function index() {
		$info['utilisateur']=$this->User_model->get_user();
        $info['content']='utilisateur';

		$data['animes']=$this->Animes_model->get_animes();
		$data['content']='animes';
		
		$this->load->vars($info);
		$this->load->vars($data);
		$this->load->view('header');
		$this->load->view('all_anime_view');
		$this->load->view('footer');
	}
}
?>
