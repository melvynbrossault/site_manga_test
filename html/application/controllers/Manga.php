<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manga extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Scan_model');
		$this->load->model('User_model');
		$this->load->helper('url');
	}
	
    public function index() {
		$info['utilisateur']=$this->User_model->get_user();
        $info['content']='utilisateur';

		$data['scan']=$this->Scan_model->get_scan();
		$data['content']='scan';
		
		$this->load->vars($info);
		$this->load->vars($data);
		$this->load->view('header');
		$this->load->view('anime_view');
		$this->load->view('footer');
	}
}
?>
