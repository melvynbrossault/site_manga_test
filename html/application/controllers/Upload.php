<?php

class Upload extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
        $this->load->model('Scan_model');
		$this->load->model('User_model');
	}

	function index()
	{
        $info['utilisateur']=$this->User_model->get_user();
        $info['content']='utilisateur';

		$data['scan']=$this->Scan_model->get_scan();
		$data['content']='scan';

        $mangas['manga']=$this->Scan_model->get_mangas();
		$mangas['content']='manga';
		
		$this->load->vars($info);
		$this->load->vars($data);
        $this->load->vars($mangas);
        $this->load->view('header');
		$this->load->view('ajout_scan', array('error' => ' ' ));
        $this->load->view('footer');
	}

	function do_upload()
	{
        $nom = $this->input->post('nom');
        $chap = $this->input->post('chapitre');
		$config['upload_path'] = './images/'.$nom;
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = '0';
		$config['max_size']	= '10000';
		$config['max_width']  = '102400';
		$config['max_height']  = '76800';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

            $this->load->view('header');
			$this->load->view('ajout_scan', $error);
            $this->load->view('footer');
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

            $this->load->view('header');
			$this->load->view('ajout_scan_success', $data);
            $this->load->view('footer');
		}
	}

    public function upload_validation()  
    {  
        $this->load->library('form_validation');  
  
        $this->form_validation->set_rules('nom', 'Nom', 'required|trim');
  
        $this->form_validation->set_rules('chapitre', 'Chapitre', 'required|trim'); 
  
    if ($this->form_validation->run())  
        {  
            $this->load->model('Scan_model');
            $this->Scan_model->ajout_correct();
            $this->load->view('header');
            $this->load->view('ajout_scan');
            $this->load->view('footer');
         }   
            else {  
              
            $this->load->view('header');
            $this->load->view('ajout_scan');  
            $this->load->view('footer');
        }  
    }  
}
?>