<?php  
defined('BASEPATH') OR exit('No direct script access allowed');  
  
class Connexion extends CI_Controller {  

    public function __construct(){
		parent::__construct();
		$this->load->helper('url');
	}
  
    public function index()  
    {  
        $this->login();  
    }  
  
    public function login()  
    {  
        $this->load->view('header');
        $this->load->view('login_view');
        $this->load->view('footer');  
    }  
  
    public function signin()  
    {  
        $this->load->view('header');
        $this->load->view('signin');
        $this->load->view('footer'); 
    }  
  
    public function data()  
    {  
        if ($this->session->userdata('currently_logged_in'))   
        {  
            $this->load->model('User_model');
            $info['utilisateur']=$this->User_model->get_user();
            $info['content']='utilisateur';
            $this->load->vars($info);
            $this->load->view('header');
            $this->load->view('data');  
            $this->load->view('footer');
        } else {  
            redirect('Connexion/invalid');  
        }  
    }  
  
    public function invalid()  
    {  
        $this->load->view('header');
        $this->load->view('invalid');  
        $this->load->view('footer');
    }  
  
    public function login_action()  
    {  
        $this->load->helper('security');  
        $this->load->library('form_validation');  
  
        $this->form_validation->set_rules('username', 'Username:', 'required|trim|xss_clean|callback_validation');  
        $this->form_validation->set_rules('password', 'Password:', 'required|trim');  
  
        if ($this->form_validation->run())   
        {  
            $data = array(  
                'username' => $this->input->post('username'),  
                'currently_logged_in' => 1  
                );
                
                $this->session->set_userdata($data);  
                redirect('Connexion/data');  
        }   
        else {  
            $this->load->view('header');
            $this->load->view('login_view'); 
            $this->load->view('footer'); 
        }  
    }  
  
    public function signin_validation()  
    {  
        $this->load->library('form_validation');  
  
        $this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[membres.username]');  
  
        $this->form_validation->set_rules('password', 'Password', 'required|trim');  
  
        $this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|trim|matches[password]');  
  
        $this->form_validation->set_message('is_unique', 'username already exists');  
  
    if ($this->form_validation->run())  
        {  
            $this->load->model('Signin_model');
            $this->Signin_model->signin_correctly();
            $this->load->view('header');
            $this->load->view('inscrit');
            $this->load->view('footer');
         }   
            else {  
              
            $this->load->view('header');
            $this->load->view('signin');  
            $this->load->view('footer');
        }  
    }  
  
    public function validation()  
    {  
        $this->load->model('Login_model');  
  
        if ($this->Login_model->log_in_correctly())  
        {  
  
            return true;  
        } else {  
            $this->form_validation->set_message('validation', 'Pseudo ou Mot de Passe incorrect.');  
            return false;  
        }  
    }  
  
    public function logout()  
    {  
        $this->session->sess_destroy();  
        redirect('Connexion/login');  
    }  
  
}  
?>  