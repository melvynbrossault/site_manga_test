<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Episode extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Animes_model');
		$this->load->model('User_model');
		$this->load->helper('url');
	}
	
    public function index() {
		$info['utilisateur']=$this->User_model->get_user();
        $info['content']='utilisateur';
		$data['episode']=$this->Animes_model->get_episodes();
		$data['content']='episode';
		
		$this->load->vars($info);
		$this->load->vars($data);
		$this->load->view('header');
		$this->load->view('episode_view');
		$this->load->view('footer');
	}

	public function ajout_validation()  
    {  
        $this->load->library('form_validation');  
  
        $this->form_validation->set_rules('nom', 'Nom', 'required|trim'); //|is_unique[animes.nom]
  
        $this->form_validation->set_rules('langue', 'Langue', 'required|trim'); 

		$this->form_validation->set_rules('episode', 'Episode', 'trim'); 

		$this->form_validation->set_rules('lien_1', 'Lien_1', 'trim'); 

		$this->form_validation->set_rules('lien_2', 'Lien_2', 'trim');

		$this->form_validation->set_message('is_unique', 'un Animé de se nom existe déjà !'); 
  
    if ($this->form_validation->run())  
        {  
            $this->Animes_model->ajout_correct_episode();
			redirect('Anime/ajout');

			$info['utilisateur']=$this->User_model->get_user();
			$info['content']='utilisateur';

			$data['anime']=$this->Animes_model->get_episodes();
			$data['content']='anime';
			
			$this->load->vars($info);
			$this->load->vars($data);
            $this->load->view('header');
            $this->load->view('ajout_anime');
            $this->load->view('footer');
         }   
            else {  
			$info['utilisateur']=$this->User_model->get_user();
			$info['content']='utilisateur';

			$data['anime']=$this->Animes_model->get_episodes();
			$data['content']='anime';
			
			$this->load->vars($info);
			$this->load->vars($data);
            $this->load->view('header');
            $this->load->view('ajout_anime');  
            $this->load->view('footer');
        }  
    }  
}
?>
