<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anime extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Animes_model');
		$this->load->model('User_model');
		$this->load->helper(array('form', 'url'));
	}
	
    public function index() {
		$info['utilisateur']=$this->User_model->get_user();
        $info['content']='utilisateur';

		$data['anime']=$this->Animes_model->get_episodes();
		$data['content']='anime';
		
		$this->load->vars($info);
		$this->load->vars($data);
		$this->load->view('header');
		$this->load->view('anime_view');
		$this->load->view('footer');
	}

	public function ajout()
	{
        $info['utilisateur']=$this->User_model->get_user();
        $info['content']='utilisateur';

		$data['anime']=$this->Animes_model->get_episodes();
		$data['content']='anime';
		
		$this->load->vars($info);
		$this->load->vars($data);
        $this->load->view('header');
		$this->load->view('ajout_anime');
        $this->load->view('footer');
	}

	public function ajout_validation()  
    {  
        $this->load->library('form_validation');  
  
        $this->form_validation->set_rules('nom', 'Nom', 'required|trim'); //|is_unique[animes.nom]
  
        $this->form_validation->set_rules('langue', 'Langue', 'required|trim'); 

		$this->form_validation->set_rules('auteur', 'Auteur', 'trim'); 

		$this->form_validation->set_rules('type', 'Type', 'trim'); 

		$this->form_validation->set_rules('genre', 'Genre', 'trim'); 

		$this->form_validation->set_rules('synopsis', 'Synopsis', 'trim'); 

		$this->form_validation->set_rules('parution', 'Parution', 'trim'); 

		$this->form_validation->set_message('is_unique', 'un Animé de se nom existe déjà !'); 
  
    if ($this->form_validation->run())  
        {  
            $this->Animes_model->ajout_correct_anime();
			redirect('Anime/ajout');

			$info['utilisateur']=$this->User_model->get_user();
			$info['content']='utilisateur';

			$data['anime']=$this->Animes_model->get_episodes();
			$data['content']='anime';
			
			$this->load->vars($info);
			$this->load->vars($data);
            $this->load->view('header');
            $this->load->view('ajout_anime');
            $this->load->view('footer');
         }   
            else {  
			$info['utilisateur']=$this->User_model->get_user();
			$info['content']='utilisateur';

			$data['anime']=$this->Animes_model->get_episodes();
			$data['content']='anime';
			
			$this->load->vars($info);
			$this->load->vars($data);
            $this->load->view('header');
            $this->load->view('ajout_anime');  
            $this->load->view('footer');
        }  
    }  
}
?>
