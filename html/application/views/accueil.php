<body class="bdy">
    <main>
        <div class="card-deck">
            <div class="card">
              <div class="card-body">
                <h3 id="mangas-img" class="card-title text-center">Mangas</h3>
                <?php if (count($list_last_scan) > 30 ) { ?>
                    <?php for($i = 0; $i < 50; ++$i) {?>  
                        <?php if($i != 0 && ($list_last_scan[$i]['nom']==$list_last_scan[$i-1]['nom'])){ ?>
                            <p class="card-text"><?php echo '<a class="liens" href="'.base_url().'index.php/Episode/index?id='.$list_last_scan[$i]['id'].'&amp;chapitre='.$list_last_scan[$i]['chapitre'].'">'.'Chapitre '.$list_last_scan[$i]['chapitre'] .'</a>';?></p>
                        <?php } else { ?>
                            <h5 class="card-title"><?php echo '<a class="liens" href="'.base_url().'index.php/Anime/index?id='.$list_last_scan[$i]['id'].'">'.$list_last_scan[$i]['nom'] .'</a>'; ?></h5>
                            <p class="card-text"><?php echo '<a class="liens" href="'.base_url().'index.php/Episode/index?id='.$list_last_scan[$i]['id'].'&amp;chapitre='.$list_last_scan[$i]['chapitre'].'">'.'Chapitre '.$list_last_scan[$i]['chapitre'] .'</a>';?></p>
                        <?php } ?>
                    <?php } ?>
                <?php } else { ?>
                    <?php for($i = 0, $max = count($list_last_scan);$i < $max; ++$i) {?>
                        <?php if($i != 0 && ($list_last_scan[$i]['nom']==$list_last_scan[$i-1]['nom'])){ ?>
                            <p class="card-text"><?php echo '<a class="liens" href="'.base_url().'index.php/Episode/index?id='.$list_last_scan[$i]['id'].'&amp;chapitre='.$list_last_scan[$i]['chapitre'].'">'.'Chapitre '.$list_last_scan[$i]['chapitre'] .'</a>';?></p>
                        <?php } else { ?>
                            <h5 class="card-title"><?php echo '<a class="liens" href="'.base_url().'index.php/Anime/index?id='.$list_last_scan[$i]['id'].'">'.$list_last_scan[$i]['nom'] . '</a>'; ?></h5>
                            <p class="card-text"><?php echo '<a class="liens" href="'.base_url().'index.php/Episode/index?id='.$list_last_scan[$i]['id'].'&amp;chapitre='.$list_last_scan[$i]['chapitre'].'">'.'Chapitre '.$list_last_scan[$i]['chapitre'] .'</a>';?></p>
                        <?php } ?>
                    <?php } ?>
                <?php }?>
              </div>
              <div class="card-footer">
                <small class="text-muted"><?php echo 'Last updated : ', $list_last_scan[0]['date_mise_en_ligne']; ?></small>
              </div>
            </div>
            <div class="card">
              <div class="card-body">
                <h3 id="animes-img" class="card-title text-center">Animés</h3>
                <?php if (count($list_last_episodes) > 30 ) { ?>
                    <?php for($i = 0; $i < 50; ++$i) {?>  
                        <?php if($i != 0 && ($list_last_episodes[$i]['nom']==$list_last_episodes[$i-1]['nom'])){ ?>
                            <p class="card-text"><?php echo '<a class="liens" href="'.base_url().'index.php/Episode/index?id='.$list_last_episodes[$i]['id'].'&amp;episode='.$list_last_episodes[$i]['episode'].'">'.'Episode '.$list_last_episodes[$i]['episode'] .' '. $list_last_episodes[$i]['langue'].'</a>';?></p>
                        <?php } else { ?>
                            <h5 class="card-title"><?php echo '<a class="liens" href="'.base_url().'index.php/Anime/index?id='.$list_last_episodes[$i]['id'].'">'.$list_last_episodes[$i]['nom'] .' '. $list_last_episodes[$i]['langue'].'</a>'; ?></h5>
                            <p class="card-text"><?php echo '<a class="liens" href="'.base_url().'index.php/Episode/index?id='.$list_last_episodes[$i]['id'].'&amp;episode='.$list_last_episodes[$i]['episode'].'">'.'Episode '.$list_last_episodes[$i]['episode'] .' '. $list_last_episodes[$i]['langue'].'</a>';?></p>
                        <?php } ?>
                    <?php } ?>
                <?php } else { ?>
                    <?php for($i = 0, $max = count($list_last_episodes);$i < $max; ++$i) {?>
                        <?php if($i != 0 && ($list_last_episodes[$i]['nom']==$list_last_episodes[$i-1]['nom'])){ ?>
                            <p class="card-text"><?php echo '<a class="liens" href="'.base_url().'index.php/Episode/index?id='.$list_last_episodes[$i]['id'].'&amp;episode='.$list_last_episodes[$i]['episode'].'">'.'Episode '.$list_last_episodes[$i]['episode'] .' '. $list_last_episodes[$i]['langue'].'</a>';?></p>
                        <?php } else { ?>
                            <h5 class="card-title"><?php echo '<a class="liens" href="'.base_url().'index.php/Anime/index?id='.$list_last_episodes[$i]['id'].'">'.$list_last_episodes[$i]['nom'] . ' '. $list_last_episodes[$i]['langue'].'</a>'; ?></h5>
                            <p class="card-text"><?php echo '<a class="liens" href="'.base_url().'index.php/Episode/index?id='.$list_last_episodes[$i]['id'].'&amp;episode='.$list_last_episodes[$i]['episode'].'">'.'Episode '.$list_last_episodes[$i]['episode'] .' '. $list_last_episodes[$i]['langue'].'</a>';?></p>
                        <?php } ?>
                    <?php } ?>
                <?php }?>
                </div>
              <div class="card-footer">
                <small class="text-muted"><?php echo 'Last updated : ', $list_last_episodes[0]['date_mise_en_ligne']; ?></small>
              </div>
            </div>
        </div>
    </main>    
</body>