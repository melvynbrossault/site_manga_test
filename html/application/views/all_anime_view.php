<body class="bdy">
<main>
      <div class="card-deck">
        <div class="card">
          <div class="card-body">
            <h3 class="card-title text-center">Tout les Animés</h3>
            <div class="card-deck">

            <?php for($i = 0, $max = count($animes);$i < $max; ++$i) {?>
              <?php
              if($i!= 0) {
                if(strcmp($animes[$i]['nom'], $animes[$i-1]['nom'])!=0) { ?>
              <div class="card boxWidth">
                <div class="card-body">
                  <h5 class="card-title text-center"><?php echo $animes[$i]['nom']; ?></h5>
                  <div id="<?php 
                    if($animes[$i]['image']==NULL){
                      echo 'error-img';
                    } else {
                      echo $animes[$i]['image'];
                    }
                    ?>">
                  <?php if(strcmp($animes[$i]['langue'], 'VF') ==0 ) {?>
                    <button class="button button-anime" onclick="window.location.href = '<?php echo base_url().'index.php/Anime/index?id='.$animes[$i]['id'] ?>';">VF</button>
                  <?php } else { ?>
                    <button class="button button-anime" onclick="window.location.href = '<?php echo base_url().'index.php/Anime/index?id='.$animes[$i]['id'] ?>';">VOSTFR</button>
                  <?php } ?>
                  <?php if(strcmp($animes[$i]['langue'], 'VOSTFR') != 0 ) { ?>
                    <?php if(strcmp($animes[$i+1]['langue'], 'VOSTFR') == 0 ) {?>
                      <button class="button button-anime" onclick="window.location.href = '<?php echo base_url().'index.php/Anime/index?id='.$animes[$i+1]['id'] ?>';">VOSTFR</button>
                    <?php } ?>
                  <?php } ?>
                </div>
                </div>
                <div class="card-footer">
                </div>
              </div>
              <?php }
              } else { 
              ?>
              <div class="card boxWidth">
                <div class="card-body">
                  <h5 class="card-title text-center"><?php echo $animes[$i]['nom']. ' '.$animes[$i]['langue']; ?></h5>
                  <div id="<?php 
                    if($animes[$i]['image']==NULL){
                      echo 'error-img';
                    } else {
                      echo $animes[$i]['image'];
                    }
                    ?>">
                    <?php if(strcmp($animes[$i]['langue'], 'VF') ==0 ) {?>
                      <button class="button button-anime" onclick="window.location.href = '<?php echo base_url().'index.php/Anime/index?id='.$animes[$i]['id'] ?>';">VF</button>
                    <?php } else { ?>
                    <button class="button button-anime" onclick="window.location.href = '<?php echo base_url().'index.php/Anime/index?id='.$animes[$i]['id'] ?>';">VOSTFR</button>
                  <?php } ?>
                    <?php if(strcmp($animes[$i+1]['langue'], 'VOSTFR') ==0 ) {?>
                      <button class="button button-anime" onclick="window.location.href = '<?php echo base_url().'index.php/Anime/index?id='.$animes[$i+1]['id'] ?>';">VOSTFR</button>
                    <?php } ?>
                  </div>
                </div>
                <div class="card-footer">
                </div>
              </div>
              <?php } ?>
            <?php } ?>

            </div>
          </div>
        </div>
    </div>
    </main>
</body>