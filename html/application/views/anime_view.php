<body class="bdy">
    <main>
    <div class="card-deck">
            <div class="card">
              <div class="card-body">
              <?php
              $i = 0;
              $res = 0;
              $max = count($anime);
              while($i < $max && $res == 0 ){ 
              ?>
                <?php if($anime[$i]['id']==$_GET['id']){ 
                  $res=1; 
                ?>
                  <h3 class="card-title text-center"><?php echo $anime[$i]['nom'], ' ', $anime[$i]['langue']; ?></h3>
                <?php }?>
              <?php 
              ++$i;
              }?>
              <?php
              $i = 0;
              $res = 0;
              $max = count($anime);
              while($i < $max && $res == 0 ){ 
              ?>
                <?php if($anime[$i]['id']==$_GET['id']){ 
                  $res=1; 
                ?>
                  <div>
                  <div class="boxWidth img-anime" id="<?php 
                    if($anime[$i]['image']==NULL){
                      echo 'error-img';
                    } else {
                      echo $anime[$i]['image'];
                    }
                    ?>">
                  </div>
                  <div id="identite">
                    <?php echo 'Auteur : '.$anime[$i]['auteur'];?><br><br>
                    <?php echo 'Type : '.$anime[$i]['type'];?><br><br>
                    <?php echo 'Genre : '.$anime[$i]['genre'];?><br><br>
                    <?php echo 'Année de production : '.$anime[$i]['parution'];?><br>
                  </div>
                </div><br>
                <p>Synopsis: <?php echo $anime[$i]['synopsis']?></p>
                <?php }?>
              <?php 
              ++$i;
              }?>
              </div>
            </div>
        </div>
        <div class="card-deck">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Liste Episodes</h5>
              <div>
              <?php for($i = 0, $max = count($anime);$i < $max; ++$i) {?>
                <?php if($anime[$i]['id']==$_GET['id']){ ?>
                  <p style="float:left;"><?php echo '<a class="liens" href="'.base_url().'index.php/Episode/index?id='.$anime[$i]['id'].'&amp;episode='.$anime[$i]['episode'].'">'.'Episode ' . ' '. $anime[$i]['episode'].'</a>'; ?></p> 
                  <p style="float:right;"><?php echo $anime[$i]['date_mise_en_ligne']; ?></p>
                  <br><br>
                <?php }?>
              <?php }?>
              </div>
              <br><br>
            </div>
          </div>
        </div>
    </main>    
</body>