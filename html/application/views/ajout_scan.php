<body class="bdy">
    <div class="card-deck">
            <div class="card">
              <div class="card-body">
                <h4 id="animes-img" class="card-title text-center">Ajout chapitre</h4>
                    <div style="display:flex; margin:auto; text-align:center; justify-content: center;" >
                    <?php  
  
                        echo form_open('Upload/upload_validation');  

                        echo validation_errors();  

                        
                        echo "<p>Manga:";
                        echo "<select name='nom'>";
                        for($i = 0, $max = count($manga);$i < $max; ++$i) {
                            echo "<option valeur=".$manga[$i]['id'].">".$i."</option>";
                        }
                        echo "</select>";
                        echo "</p>";  

                        echo "<p>Chapitre:";  
                        echo "<input type='number'  name='chapitre' min='1' max='20000'/>";
                        echo "</p>"; 
                        
                        /*
                        echo "<p>Nom:";  
                        echo form_input('nom');
                        echo "</p>";

                        echo "<p>Chapitre:";  
                        echo form_input('chapitre');
                        echo "</p>";
                        */

                        echo "<p>";  
                        echo form_submit('submit_scan', 'Ajout');  
                        echo "</p>";  

                        echo form_close();  

                        ?>  
                    </div>
              </div>
              <div class="card-footer">
              </div>
            </div>
            <div class="card">
              <div class="card-body">
                <h4 id="mangas-img" class="card-title text-center">Ajout image lié chapitre</h4>
                    <div style="display:flex; margin:auto; text-align:center; justify-content: center;" >
                    <?php  
                        
                        echo $error;

                        echo  form_open_multipart('upload/do_upload');


                        
                        echo "<p>Manga:";
                        echo "<select name='nom'>";
                        for($i = 0, $max = count($manga);$i < $max; ++$i) {
                            echo "<option valeur=".$manga[$i]['id'].">".$manga[$i]['nom']."</option>";
                        }
                        echo "</select>";
                        echo "</p>";  

                        echo "<p>Chapitre:";  
                        echo "<input type='number'  name='chapitre' min='1' max='20000'/>";
                        echo "</p>"; 
                        
                        echo "<p>Fichier:";  
                        echo "<input type='file' name='userfile' size='10000' />";
                        echo "</p>"; 

                        echo "<p>";  
                        echo form_submit('submit_scan', 'Ajout');  
                        echo "</p>";  

                        echo form_close();  

                        ?>  
                    </div>
              </div>
              <div class="card-footer">
              </div>
            </div>
    </div>
</body>