<body class="bdy">
    <div class="card-deck">
            <div class="card">
              <div class="card-body">
                <h4 id="mangas-img" class="card-title text-center">Connexion</h4>
                    <div style="display:flex; margin:auto; text-align:center; justify-content: center;" >
                    <?php  
    
                        echo form_open('Connexion/login_action');  
                    
                        echo validation_errors();  
                    
                        echo "<p>Pseudo: ";  
                        echo form_input('username', $this->input->post('username'));  
                        echo "</p>";  
                    
                        echo "<p>Mot de Passe: ";  
                        echo form_password('password');  
                        echo "</p>";  
                    
                        echo "</p>";  
                        echo form_submit('login_submit', 'Connexion');  
                        echo "</p>";  
                    
                        echo form_close();  
                    
                        ?>  
                    </div>
              </div>
              <div class="card-footer">
              </div>
            </div>
    </div>
</body>