<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../css/style.css"/>
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
</head>
    <header id="top">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a href="<?php echo base_url();?>"><img src="" title="logo" alt="logo" /></a> <!--width="" height="" -->
            <a class="navbar-brand" href="<?php echo base_url();?>">Scan & Anime</a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item btn btn-secondary"><a class="nav-link" href="<?php echo base_url();?>">Accueil</a>
                    </li><br>
                    <li class="nav-item btn btn-secondary"><a class="nav-link" href="mangas.html">Mangas</a>
                    </li><br>
                    <li class="nav-item btn btn-secondary"><a class="nav-link" href="<?php echo base_url().'index.php/All_anime/index'; ?>">Animés</a>
                    </li><br>
                </ul>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="nav navbar-nav">
                <?php if ($this->session->currently_logged_in == 0){ ?>
                        <input type="button" style="margin:5px;" value="Connexion" onclick="window.location.href = '<?php echo base_url();?>index.php/Connexion'">
                        <input type="button" style="margin:5px;" value="Inscription" onclick="window.location.href = '<?php echo base_url();?>index.php/Connexion/signin'">
                    <?php }else{ ?>
                        <div class="justify-content-center col">
                    <p>Connecté en tant que : <?php echo $this->session->username; ?></p>
                        </div>
                        <?php for($i = 0, $max = count($utilisateur);$i < $max; ++$i) {?>
                            <?php if ( strcmp($this->session->username, $utilisateur[$i]['username'])==0 ){ ?>
                                <?php if ( strcmp($utilisateur[$i]['niveau'], 'fondateur')==0 ){ ?>
                                    <input type="button" style="margin:5px;" value="FONDATEUR" onclick="window.location.href = '<?php echo base_url();?>index.php/Accueil/admin_view'">
                                    <input type="button" style="margin:5px;" value="Deconnexion" onclick="window.location.href = '<?php echo base_url();?>index.php/Connexion/logout'">
                                <?php }else{ ?>
                                    <input type="button" value="Mon Profil" onclick="window.location.href = '<?php echo base_url();?>index.php/Profil/index'">
                                    <input type="button" value="Deconnexion" onclick="window.location.href = '<?php echo base_url();?>index.php/Connexion/logout'">
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
        </nav>
    </header>

