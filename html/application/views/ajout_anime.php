<body class="bdy">
    <div class="card-deck">
            <div class="card">
              <div class="card-body">
                <h4 id="animes-img" class="card-title text-center">Ajout Animé</h4>
                    <div style="display:flex; margin:auto; text-align:center; justify-content: center;" >
                    <?php  
  
                        echo form_open('Anime/ajout_validation');  

                        echo validation_errors();  

                        echo "<p>Nom:";  
                        echo form_input('nom');
                        echo "</p>";
                        
                        echo "<p>langue:"; //ça c'est un set dans ma bdd
                        echo "<select name='langue'>";
                            echo "<option valeur='VF'>VF</option>";
                            echo "<option valeur='VOSTFR'>VOSTFR</option>";
                        echo "</select>";
                        echo "</p>";

                        echo "<p>Auteur:";  
                        echo form_input('auteur');
                        echo "</p>";

                        echo "<p>type:";  //ça c'est un enum
                        echo form_input('type');
                        echo "</p>";

                        echo "<p>Genre:";  
                        echo form_input('genre');
                        echo "</p>";

                        echo "<p>Synopsis:";  
                        echo form_input('synopsis');
                        echo "</p>";

                        echo "<p>Année parution:";  //ça c'est un year
                        echo "<input type='number'  name='parution' min='1900' max='3000'/>";
                        echo "</p>";

                        echo "<p>";  
                        echo form_submit('submit_anime', 'Ajout');  
                        echo "</p>";  

                        echo form_close();  

                        ?>  
                    </div>
              </div>
              <div class="card-footer">
              </div>
            </div>
            <div class="card">
              <div class="card-body">
                <h4 id="mangas-img" class="card-title text-center">Ajout episode</h4>
                    <div style="display:flex; margin:auto; text-align:center; justify-content: center;" >
                    <?php  
                        
                        echo form_open('Episode/ajout_validation');

                        echo validation_errors();  


                        
                        echo "<p>Anime:";
                        echo "<select name='nom'>";
                        for($i = 0, $max = count($anime);$i < $max; ++$i) {
                          if(strcmp($anime[$i]['nom'], $anime[$i-1]['nom'])!=0){
                            echo "<option valeur=".$anime[$i]['id'].">".$anime[$i]['nom']."</option>";
                          }
                        }
                        echo "</select>";
                        echo "</p>";  

                        echo "<p>langue:"; //ça c'est un set dans ma bdd
                        echo "<select name='langue'>";
                            echo "<option valeur='VF'>VF</option>";
                            echo "<option valeur='VOSTFR'>VOSTFR</option>";
                        echo "</select>";
                        echo "</p>";

                        echo "<p>Episode:";  
                        echo "<input type='number'  name='episode' min='1' max='20000'/>";
                        echo "</p>"; 
                        
                        echo "<p>Lien 1:";  
                        echo form_input('lien_1');
                        echo "</p>"; 

                        echo "<p>Lien 2:";  
                        echo form_input('lien_2');
                        echo "</p>"; 

                        echo "<p>";  
                        echo form_submit('submit_episode', 'Ajout');  
                        echo "</p>";  

                        echo form_close();  

                        ?>  
                    </div>
              </div>
              <div class="card-footer">
              </div>
            </div>
    </div>
</body>